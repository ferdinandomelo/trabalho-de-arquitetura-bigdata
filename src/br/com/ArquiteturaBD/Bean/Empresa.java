package br.com.ArquiteturaBD.Bean;

public class Empresa {

	private String iDEmpresa;
	private String nomeEmpresa;
	

	public Empresa(String iDEmpresa, String nomeEmpresa) {
		super();
		this.iDEmpresa = iDEmpresa;
		this.nomeEmpresa = nomeEmpresa;
	}

	public Empresa() {
		// TODO Auto-generated constructor stub
	}

	public String getiDEmpresa() {
		return iDEmpresa;
	}

	public void setiDEmpresa(String iDEmpresa) {
		this.iDEmpresa = iDEmpresa;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

}
