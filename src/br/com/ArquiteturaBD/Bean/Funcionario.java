package br.com.ArquiteturaBD.Bean;

public class Funcionario {

	private String iDFuncionario;
	private String nomeFuncionario;
	private String CPFFuncionario;
	private String cargoFuncionario;
	private float salarioFuncionario;
	private boolean funcionarioAtivo;
	private String dataAdmissao;
	private Empresa empresaFuncionario;
	
	private int iDEmpresa;

	public Funcionario(String iDFuncionario, String nomeFuncionario, String cPFFuncionario, String cargoFuncionario,
			float salarioFuncionario, boolean funcionarioAtivo, String dataAdmissao, Empresa empresaFuncionario) {
		super();
		this.iDFuncionario = iDFuncionario;
		this.nomeFuncionario = nomeFuncionario;
		CPFFuncionario = cPFFuncionario;
		this.cargoFuncionario = cargoFuncionario;
		this.salarioFuncionario = salarioFuncionario;
		this.funcionarioAtivo = funcionarioAtivo;
		this.dataAdmissao = dataAdmissao;
		this.setEmpresaFuncionario(empresaFuncionario);
	}

	public Funcionario(int iDEmpresa) {
		this.setiDEmpresa(iDEmpresa);
	}

	public String getiDFuncionario() {
		return iDFuncionario;
	}

	public void setiDFuncionario(String iDFuncionario) {
		this.iDFuncionario = iDFuncionario;
	}

	public String getNomeFuncionario() {
		return nomeFuncionario;
	}

	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}

	public String getCPFFuncionario() {
		return CPFFuncionario;
	}

	public void setCPFFuncionario(String cPFFuncionario) {
		CPFFuncionario = cPFFuncionario;
	}

	public String getCargoFuncionario() {
		return cargoFuncionario;
	}

	public void setCargoFuncionario(String cargoFuncionario) {
		this.cargoFuncionario = cargoFuncionario;
	}

	public float getSalarioFuncionario() {
		return salarioFuncionario;
	}

	public void setSalarioFuncionario(float salarioFuncionario) {
		this.salarioFuncionario = salarioFuncionario;
	}

	public boolean isFuncionarioAtivo() {
		return funcionarioAtivo;
	}

	public void setFuncionarioAtivo(boolean funcionarioAtivo) {
		this.funcionarioAtivo = funcionarioAtivo;
	}

	public String getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(String dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public Empresa getEmpresaFuncionario() {
		return empresaFuncionario;
	}

	public void setEmpresaFuncionario(Empresa empresaFuncionario) {
		this.empresaFuncionario = empresaFuncionario;
	}

	public int getiDEmpresa() {
		return iDEmpresa;
	}

	public void setiDEmpresa(int iDEmpresa) {
		this.iDEmpresa = iDEmpresa;
	}

	
}
