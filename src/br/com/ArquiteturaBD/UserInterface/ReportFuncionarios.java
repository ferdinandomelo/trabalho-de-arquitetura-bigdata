package br.com.ArquiteturaBD.UserInterface;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;

import br.com.ArquiteturaBD.Bean.Empresa;
import br.com.ArquiteturaBD.Bean.Funcionario;
import br.com.ArquiteturaBD.Database.Mongoconnector;

import javax.swing.JTable;

public class ReportFuncionarios extends JFrame {

	private JPanel contentPane;
	private JTable tableFuncionarios;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ReportFuncionarios frame = new ReportFuncionarios();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ReportFuncionarios() {
		setTitle("Relatório");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		DefaultTableModel model = new DefaultTableModel();
		String[] columnNames = { "id Funcionario", "Empresa", "Nome", "CPF", "Cargo", "Salario", "Funcionário Ativo",
				"DataAdmissao" };
		model.addColumn(columnNames[0].toString());
		model.addColumn(columnNames[1].toString());
		model.addColumn(columnNames[2].toString());
		model.addColumn(columnNames[3].toString());
		model.addColumn(columnNames[4].toString());
		model.addColumn(columnNames[5].toString());
		model.addColumn(columnNames[6].toString());
		model.addColumn(columnNames[7].toString());
		tableFuncionarios = new JTable(model);
		tableFuncionarios.setColumnSelectionAllowed(true);
		tableFuncionarios.setCellSelectionEnabled(true);
		contentPane.add(tableFuncionarios, BorderLayout.CENTER);

		Mongoconnector dbConnector = new Mongoconnector();

		FindIterable<Document> doc;
		doc = dbConnector.collFuncionarios.find();

		model.addRow(columnNames);

		for (Document document2 : doc) {
			BasicDBObject documentEmpresa = new BasicDBObject();
			documentEmpresa.put("_id", document2.get("iDEmpresa").toString());

			FindIterable<Document> intEmp = dbConnector.collEmpresas.find(documentEmpresa);
			Empresa empresaFunc = new Empresa();
			for (Document empresa : intEmp) {
				empresaFunc.setiDEmpresa(empresa.get("_id").toString());
				empresaFunc.setNomeEmpresa(empresa.get("Nome").toString());
				System.out.println("teste" + empresaFunc.getNomeEmpresa());
			}

			Funcionario f = new Funcionario(document2.get("_id").toString(), document2.get("Nome").toString(),
					document2.get("CPF").toString(), document2.get("Cargo").toString(),
					Float.parseFloat(document2.get("Salario").toString()),
					Boolean.parseBoolean((String) document2.get("Ativo")), document2.get("DataAdmissao").toString(),
					empresaFunc);
			String ativo;
			if (f.isFuncionarioAtivo() == true) {
				ativo = "Ativo";
			} else {
				ativo = "Desligado";
			}
			// inserindo os valores na tabela
			model.addRow(new Object[] { f.getiDFuncionario(), f.getEmpresaFuncionario().getNomeEmpresa(),
					f.getNomeFuncionario(), f.getCPFFuncionario(), f.getCargoFuncionario(), f.getSalarioFuncionario(),
					ativo, f.getDataAdmissao() });
		}

	}

}
