package br.com.ArquiteturaBD.UserInterface;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.model.InsertOneOptions;

import br.com.ArquiteturaBD.Bean.Empresa;
import br.com.ArquiteturaBD.Bean.Funcionario;
import br.com.ArquiteturaBD.Database.Mongoconnector;

public class NovoCadastroUserInterface {

	private JFrame frmConsultaFuncionrio;
	private JTextField txtId;
	private JTextField txtNome;
	private JTextField txtCpf;
	private JTextField txtEmpresaDoFuncionario;
	private JLabel lblCargo;
	private JTextField txtCargo;
	private JLabel lblSalario;
	private JTextField txtSalarioEmReais;
	private JLabel lblFuncionarioAtivo;
	private JTextField txtDataDeAdmisso;
	private JButton btnGravar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NovoCadastroUserInterface window = new NovoCadastroUserInterface();
					window.frmConsultaFuncionrio.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NovoCadastroUserInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmConsultaFuncionrio = new JFrame();
		frmConsultaFuncionrio.setTitle("Cadastro Funcionário");
		frmConsultaFuncionrio.setBounds(100, 100, 450, 300);
		frmConsultaFuncionrio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmConsultaFuncionrio.getContentPane().setLayout(null);

		JLabel lblIdDoFuncionario = new JLabel("Id do Funcionario:");
		lblIdDoFuncionario.setBounds(12, 32, 133, 15);
		frmConsultaFuncionrio.getContentPane().add(lblIdDoFuncionario);

		txtId = new JTextField();
		txtId.setEditable(false);
		txtId.setText("Id");
		txtId.setBounds(187, 30, 216, 19);
		frmConsultaFuncionrio.getContentPane().add(txtId);
		txtId.setColumns(10);

		JLabel lblNomeDoFuncionario = new JLabel("Nome do Funcionario:");
		lblNomeDoFuncionario.setBounds(12, 55, 164, 15);
		frmConsultaFuncionrio.getContentPane().add(lblNomeDoFuncionario);

		txtNome = new JTextField();
		txtNome.setText("Nome");
		txtNome.setBounds(187, 53, 216, 19);
		frmConsultaFuncionrio.getContentPane().add(txtNome);
		txtNome.setColumns(10);

		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setBounds(12, 80, 66, 15);
		frmConsultaFuncionrio.getContentPane().add(lblCpf);

		txtCpf = new JTextField();
		txtCpf.setText("CPF");
		txtCpf.setBounds(187, 78, 216, 19);
		frmConsultaFuncionrio.getContentPane().add(txtCpf);
		txtCpf.setColumns(10);

		JLabel lblEmpresa = new JLabel("Empresa:");
		lblEmpresa.setBounds(12, 12, 66, 15);
		frmConsultaFuncionrio.getContentPane().add(lblEmpresa);

		txtEmpresaDoFuncionario = new JTextField();
		txtEmpresaDoFuncionario.setText("Empresa do Funcionario");
		txtEmpresaDoFuncionario.setBounds(187, 12, 216, 17);
		frmConsultaFuncionrio.getContentPane().add(txtEmpresaDoFuncionario);
		txtEmpresaDoFuncionario.setColumns(10);

		lblCargo = new JLabel("Cargo:");
		lblCargo.setBounds(12, 99, 66, 15);
		frmConsultaFuncionrio.getContentPane().add(lblCargo);

		txtCargo = new JTextField();
		txtCargo.setText("Cargo");
		txtCargo.setBounds(187, 97, 216, 19);
		frmConsultaFuncionrio.getContentPane().add(txtCargo);
		txtCargo.setColumns(10);

		lblSalario = new JLabel("Salario:");
		lblSalario.setBounds(12, 123, 66, 15);
		frmConsultaFuncionrio.getContentPane().add(lblSalario);

		txtSalarioEmReais = new JTextField();
		txtSalarioEmReais.setText("Salario em reais");
		txtSalarioEmReais.setBounds(187, 121, 216, 19);
		frmConsultaFuncionrio.getContentPane().add(txtSalarioEmReais);
		txtSalarioEmReais.setColumns(10);

		lblFuncionarioAtivo = new JLabel("Funcionario Ativo:");
		lblFuncionarioAtivo.setBounds(12, 147, 152, 15);
		frmConsultaFuncionrio.getContentPane().add(lblFuncionarioAtivo);

		JCheckBox chckbxAtivo = new JCheckBox("Ativo");
		chckbxAtivo.setSelected(true);
		chckbxAtivo.setBounds(187, 143, 126, 23);
		frmConsultaFuncionrio.getContentPane().add(chckbxAtivo);

		JLabel lblDataDeAdmisso = new JLabel("Data de admissão:");
		lblDataDeAdmisso.setBounds(12, 174, 152, 15);
		frmConsultaFuncionrio.getContentPane().add(lblDataDeAdmisso);

		txtDataDeAdmisso = new JTextField();
		txtDataDeAdmisso.setText("Data de admissão");
		txtDataDeAdmisso.setBounds(187, 174, 216, 19);
		frmConsultaFuncionrio.getContentPane().add(txtDataDeAdmisso);
		txtDataDeAdmisso.setColumns(10);

		JButton btnCancela = new JButton("Cancela");
		btnCancela.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmConsultaFuncionrio.dispose();
			}
		});
		btnCancela.setBounds(79, 216, 114, 25);
		frmConsultaFuncionrio.getContentPane().add(btnCancela);

		btnGravar = new JButton("Gravar");
		btnGravar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Mongoconnector dbConnector = new Mongoconnector();

				BasicDBObject document = new BasicDBObject();
				document.put("CPF", txtCpf.getText());
				FindIterable<Document> doc;
				doc = dbConnector.collFuncionarios.find(document);
				System.out.println(txtCpf.getText());
				if (doc.first() != null) {
					JOptionPane.showMessageDialog(null, "Um registro foi encontrado", "Erro de cadastro",
							JOptionPane.INFORMATION_MESSAGE);
					for (Document document2 : doc) {
						BasicDBObject documentEmpresa = new BasicDBObject();
						documentEmpresa.put("_id", document2.get("iDEmpresa").toString());

						FindIterable<Document> intEmp = dbConnector.collEmpresas.find(documentEmpresa);
						Empresa empresaFunc = new Empresa();
						for (Document empresa : intEmp) {
							empresaFunc.setiDEmpresa(empresa.get("_id").toString());
							empresaFunc.setNomeEmpresa(empresa.get("Nome").toString());
							System.out.println("teste" + empresaFunc.getNomeEmpresa());
						}
						documentEmpresa.put("_id", txtEmpresaDoFuncionario.getText());

						intEmp = dbConnector.collEmpresas.find(documentEmpresa);
						for (Document empresa : intEmp) {
							empresaFunc.setiDEmpresa(empresa.get("_id").toString());
							empresaFunc.setNomeEmpresa(empresa.get("Nome").toString());
						}

						Funcionario f = new Funcionario(document2.get("_id").toString(),
								document2.get("Nome").toString(), document2.get("CPF").toString(),
								document2.get("Cargo").toString(),
								Float.parseFloat(document2.get("Salario").toString()),
								Boolean.parseBoolean((String) document2.get("Ativo")),
								document2.get("DataAdmissao").toString(), empresaFunc);

						// inserindo os valores no form
						txtEmpresaDoFuncionario.setText(f.getEmpresaFuncionario().getNomeEmpresa());
						txtId.setText(f.getiDFuncionario());
						txtNome.setText(f.getNomeFuncionario());
						txtCpf.setText(f.getCPFFuncionario());
						txtCargo.setText(f.getCargoFuncionario());
						txtSalarioEmReais.setText(String.valueOf(f.getSalarioFuncionario()));
						chckbxAtivo.setSelected(f.isFuncionarioAtivo());
						txtDataDeAdmisso.setText(f.getDataAdmissao());

					}
				} else {
					BasicDBObject documentToUpdate = new BasicDBObject();
					documentToUpdate.put("CPF", txtCpf.getText());
					Document documentValues = new Document();

					BasicDBObject documentEmpresa = new BasicDBObject();
					documentEmpresa.put("Nome", txtEmpresaDoFuncionario.getText());

					FindIterable<Document> intEmp = dbConnector.collEmpresas.find(documentEmpresa);
					Empresa empresaFunc = new Empresa();
					for (Document empresa : intEmp) {
						empresaFunc.setiDEmpresa(empresa.get("_id").toString());
						empresaFunc.setNomeEmpresa(empresa.get("Nome").toString());
						System.out.println("teste" + empresaFunc.getNomeEmpresa());
					}

					documentValues.put("iDEmpresa", empresaFunc.getiDEmpresa());
					documentValues.put("Nome", txtNome.getText());
					documentValues.put("CPF", txtCpf.getText());
					documentValues.put("Cargo", txtCargo.getText());
					documentValues.put("Salario", txtSalarioEmReais.getText());
					documentValues.put("Ativo", String.valueOf(chckbxAtivo.isSelected()));
					documentValues.put("DataAdmissao", txtDataDeAdmisso.getText());

//					gravando no banco
					try {
						dbConnector.getCollOptionsFuncionarios().validator(documentValues);
						dbConnector.collFuncionarios.insertOne(documentValues);
						JOptionPane.showMessageDialog(null,
								"O registro do funcionário " + txtNome.getText() + " foi cadastrado",
								"Ccadastro efetuado", JOptionPane.INFORMATION_MESSAGE);
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, "Um erro foi encontrado", "Erro de cadastro",
								JOptionPane.INFORMATION_MESSAGE);
					}
				}

			}
		});
		btnGravar.setBounds(268, 216, 114, 25);
		frmConsultaFuncionrio.getContentPane().add(btnGravar);
	}

	public void setVisibility(boolean b) {
		frmConsultaFuncionrio.setVisible(b);
	}
}
