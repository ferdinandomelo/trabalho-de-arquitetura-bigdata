package br.com.ArquiteturaBD.UserInterface;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;

public class PrincipalUserInterface {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrincipalUserInterface window = new PrincipalUserInterface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PrincipalUserInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 909, 402);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnCadastroDeFuncionarios = new JMenu("Cadastro de Funcionarios");
		menuBar.add(mnCadastroDeFuncionarios);
		
		JMenuItem mntmConsultaCadastro = new JMenuItem("Consulta Cadastro");
		mntmConsultaCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConsultaUserInterface frame = new ConsultaUserInterface();
				frame.setVisibility(true);
			}
		});
		mnCadastroDeFuncionarios.add(mntmConsultaCadastro);
		
		JMenuItem mntmNovoCadastro = new JMenuItem("Novo Cadastro");
		mntmNovoCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				NovoCadastroUserInterface frame = new NovoCadastroUserInterface();
				frame.setVisibility(true);
			}
		});
		mnCadastroDeFuncionarios.add(mntmNovoCadastro);
		
		JMenuItem mntmExclusoDeCadastro = new JMenuItem("Exclusão de Cadastro");
		mntmExclusoDeCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ExcluirCadastroUserInterface frame = new ExcluirCadastroUserInterface();
				frame.setVisibility(true);
			}
		});
		mnCadastroDeFuncionarios.add(mntmExclusoDeCadastro);
		
		JMenu mnRelatrioDeFuncionrios = new JMenu("Relatório de Funcionários");
		menuBar.add(mnRelatrioDeFuncionrios);
		
		JMenuItem mntmRelatorio = new JMenuItem("Relatório");
		mntmRelatorio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ReportFuncionarios frame = new ReportFuncionarios();
				frame.setVisible(true);
			}
		});
		mnRelatrioDeFuncionrios.add(mntmRelatorio);
		
		JLabel lblImagemCentral = new JLabel("");
		lblImagemCentral.setVerticalAlignment(SwingConstants.TOP);
		lblImagemCentral.setHorizontalAlignment(SwingConstants.CENTER);
		lblImagemCentral.setIcon(new ImageIcon("/home/dell/eclipse-workspace/Arquitetura de Big Data - MongoDB/Resources/Imagens/Claudio Cru Logo.jpg"));
		frame.getContentPane().add(lblImagemCentral, BorderLayout.CENTER);
	}

}
