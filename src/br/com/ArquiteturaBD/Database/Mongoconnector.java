package br.com.ArquiteturaBD.Database;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.InsertOneOptions;
import com.mongodb.client.model.ValidationOptions;

public class Mongoconnector {

	// Conecta com o banco
	// sem parametros, conecta com o localHost na porta 27017
	MongoClient mongoClient = new MongoClient();

	// Recupera o db utilizado
	MongoDatabase database = mongoClient.getDatabase("rhsolutions");

	// Recupera as collections
	public MongoCollection<Document> collFuncionarios = database.getCollection("funcionarios");

	public MongoCollection<Document> getCollFuncionarios() {
		return collFuncionarios;
	}

	public MongoCollection<Document> collEmpresas = database.getCollection("empresas");

	public MongoCollection<Document> getCollEmpresas() {
		return collEmpresas;
	}

	// Regra de validação de inclusão
	ValidationOptions collOptionsFuncionarios = new ValidationOptions().validator(Filters.and(/*Filters.exists("_id"), Filters.regex("_id", pattern),*/
			Filters.exists("Nome"),Filters.regex("Nome","\\w" ), Filters.exists("CPF"), Filters.regex("CPF","^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$" ), Filters.exists("Cargo"), Filters.regex("Cargo","\\w" ), Filters.exists("Salario"),Filters.regex("Salario","^([+-]?\\\\d*\\\\.?\\\\d*)$" ),
			Filters.exists("Ativo"), Filters.exists("DataAdmissao"),Filters.regex("DataAdmissao","dd/MM/yyyy" ), Filters.exists("ID_Empresa")));
	ValidationOptions collOptionsEmpresas = new ValidationOptions()
			.validator(Filters.and(Filters.exists("_id"), Filters.exists("Nome")));

	public ValidationOptions getCollOptionsFuncionarios() {
		return collOptionsFuncionarios;
	}

	// Printa o resultado ***somente para teste
	Block<Document> printBlock = new Block<Document>() {
		@Override
		public void apply(final Document document) {
			document.toJson();
			System.out.println(document.toJson());
			System.out.println(document.get("Nome"));
		}
	};

	public Block<Document> getPrintBlock() {
		return printBlock;
	}

}
